#!/usr/bin/env python3
import argparse
import subprocess
import socket
"""

  Name: build_ansible_hosts.py
Author: Richard Gray <richard@rgray.info>
Version: 1.0

Description: 

Scans a network using masscan and build a hosts.yml based on it. 

"""

def scan_network(network):
    """
    Use masscan to scan a given network for hosts that respond to ICMP Echo.
    :param network: IP network address and subnet mask to scan (e.g. 10.0.0.0/24).
    :return: Masscan output as a string.
    """
    cmd = f"sudo masscan {network} -p 0 --rate 1000 --wait 0 --ping"
    output = subprocess.check_output(cmd, shell=True, text=True)
    return output

def build_hosts(output):
    """
    Parse the output of a masscan scan to extract IP addresses that responded to ICMP Echo.
    :param output: Masscan output as a string.
    :return: List of IP addresses that responded to ICMP Echo.
    """
    hosts = []
    for line in output.splitlines():
        if line.startswith('Discovered'):
            ip = line.split(' ')[5].strip()
            hosts.append(ip)
    return hosts

def write_hosts_file(hosts, filename, username=None):
    """
    Write an Ansible hosts file using a list of IP addresses.
    :param hosts: List of IP addresses to write to the hosts file.
    :param filename: Name of the hosts file to write.
    :param username: Optional username to use for Ansible connections.
    """
    with open(filename, 'w') as f:
        f.write("---\n")
        f.write("all:\n")
        f.write("  hosts:\n")
        for host in hosts:
            # Attempt to resolve the host name from the IP address
            try:
                name = socket.gethostbyaddr(host)[0]
            except (socket.herror, socket.gaierror):
                name = None

            # Write the IP address and, if available, the host name to the hosts file
            f.write(f"    {host}:\n")
            if name is not None:
                f.write(f"      ansible_host: {name.split('.')[0]}\n")
            if username is not None:
                f.write(f"      ansible_user: {username}\n")

def main():
    parser = argparse.ArgumentParser(description='Scan a network with masscan and write an Ansible hosts file.')
    parser.add_argument('network', help='IP network address and subnet mask to scan (e.g. 10.0.0.0/24).')
    parser.add_argument('-u', '--username', help='Optional username to use for Ansible connections.')
    args = parser.parse_args()

    # Scan the network and build a list of hosts that responded to ICMP Echo
    output = scan_network(args.network)
    hosts = build_hosts(output)

    # Write the list of hosts to an Ansible hosts file
    write_hosts_file(hosts, 'hosts.yml', args.username)

if __name__ == '__main__':
    main()
