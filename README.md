# Ansible Automation Tools

This is a collection of tools designed to help automate the Ansible configuration process. These tools are intended to make it easier to set up and manage Ansible configurations, reducing the time and effort required to get started with automation.

## Installation

To use these tools, you will need to have Python and Ansible installed on your system. You can then download the tools from this GitLab repository:

```
git clone https://gitlab.com/thegrayman/ansible-automation-tools.git

```

## Usage

Each tool in this collection is designed to automate a specific part of the Ansible configuration process. Here is an overview of the tools included in this collection:

- `build-ansible-hosts.py`: This tool scans a network using Masscan to identify hosts that are responding to ICMP Echo requests. It then generates an Ansible hosts file based on the discovered hosts, including optional parameters for the `ansible_user` field.

- `ansible-playbook-template.py`: This tool generates a basic Ansible playbook structure, including standard sections for hosts, tasks, and handlers. The playbook can be customized as needed to fit the specific requirements of each automation task.

## Contributing

If you have a tool that you would like to contribute to this collection, please submit a merge request to this repository. All contributions are welcome and appreciated!

## License

This software is licensed under the MIT License. See LICENSE.txt for details.
