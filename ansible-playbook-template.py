#!/usr/bin/env python3

import argparse
import os

"""

  Name: ansible-playbook-template.py
Author: Richard Gray <richard@rgray.info>
Version: 1.0

Description: 

Builds a basic ansible playbook template. 

"""

def generate_playbook(playbook_name, inventory, tags):
    """
    Generate a basic Ansible playbook structure.
    :param playbook_name: The name of the playbook to generate.
    :param inventory: The path to the inventory file for the playbook.
    :param tags: A list of tags to apply to the playbook tasks.
    """
    # Generate the playbook string with placeholders for hosts, tasks, handlers, and tags
    playbook = f"""---
- name: {playbook_name}
  hosts: all
  gather_facts: yes

  tasks:

  handlers:

  tags: {', '.join(tags)}

  vars:

  # Import variables from group_vars or host_vars if needed

  # Use roles if needed

  # Include other playbooks if needed

  # Add customizations below this line"""

    # Write the playbook to a file with the specified name
    with open(f"{playbook_name}.yml", "w") as f:
        f.write(playbook)

    # Check if the inventory file exists and print a message
    if os.path.exists(inventory):
        print(f"Playbook generated in {playbook_name}.yml with inventory file {inventory}")
    else:
        print(f"Inventory file {inventory} does not exist")

def main():
    # Set up command line argument parser
    parser = argparse.ArgumentParser(description='Generate a basic Ansible playbook structure.')
    parser.add_argument('playbook_name', help='The name of the playbook to generate.')
    parser.add_argument('inventory', help='The path to the inventory file for the playbook.')
    parser.add_argument('-t', '--tags', nargs='+', default=['all'], help='A list of tags to apply to the playbook tasks.')
    args = parser.parse_args()

    # Call the generate_playbook function with the specified arguments
    generate_playbook(args.playbook_name, args.inventory, args.tags)

if __name__ == '__main__':
    main()

